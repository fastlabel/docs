# Docs

Public document pages for FastLabel

## How To Use

```bash
# Run local server to preview site.
docsify serve public
```

## Library

- [docsify](https://docsify.js.org/#/)
