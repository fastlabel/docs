# Quick Start

We will assume that we are annotating an animal object detection model. Let's start our annotation project by following the steps below.

1. [Create Project](/quick-start/?id=create-project)
2. [Labelling](/quick-start/?id=labelling)
3. [Export Labels](/quick-start/?id=export-labels)

> You must have admin privileges to create a project.

## Create Project

### Add Project

After logging in, click the "Add Project" button in the upper right corner of the screen. You will be taken to the Add Project screen, where you can enter the required information.

?> Annotation Type: Image <br />
Name: Sample Project<br />
Detail：Object Detection Model<br />

After filling out the form, click the "Create Project" button at the bottom of the screen to add a new project.

![new-project](images/new-project.png ":size=600x350")

### Add Class

Next, we need to set up the class to be used for annotation. Follow the steps below to open the Add Class screen.

1. Click on "Class" in the side menu.
2. Click the "Add Class" button in the upper right corner of the screen.

The class can have a name, code and meta information. Let's add a class for "Dog" as follows

?> Type: BBox<br />
Name: Dog <br />
Code: dog

![add-class](images/add-class.png ":size=600x350")

Then add a "Cat" class as well.

?> Type: BBox<br />
Name: Cat <br />
Code: cat

Now, we complete the class setup.

### Add Dataset

Finally, we will add a dataset. Follow these steps to open the Add Dataset screen.

1. Click on "Datasets" in the side menu
2. Click the "Add Data" button in the upper right corner of the screen.

![add-data](images/add-data.png ":size=600x350")

There are two types of data uploads: local or in seamless integration with cloud storage. In this case, we are going to upload the data locally. Follow the steps below to upload this file. (If you don't have an image of an animal, you can download and use this image.

![sample-cat-iamge](images/sample-cat-image.jpg ":size=600x350")

1. Click on the "Upload Image" button.
2. Select the picture of the cat

Once the data is uploaded, your project is set up.

## Labelling

### Start Labelling

Once your project is set up, it's time to start labeling it. Go back to the dashboard and press the "Start Labeling" button to open the annotations screen so you can start labeling. It will automatically get the tasks with the set job size (default 10).

![dashboard](images/dashboard.png ":size=600x350")

To label a rectangle (bounding box), follow these steps:

1. Select a class from the right side menu
2. Hold down the start point with the mouse and drag
3. Release the press with the mouse at the end point

![annotation-cat](images/annotation-cat.png ":size=600x350")

When you are done labeling, click on the "Submit" button in the bottom right corner of the screen. You are now ready to annotate your new image.

## Export Labels

### Download Labels

Finally, output the label results. You can download the labels by following these steps

1. Click on "Export" in the side menu
2. Click the "Export Labels" button at the bottom of the screen.

When you click the "Export Labels" button, the label result is output in JSON format. The following is a sample of the JSON output results.

```json
[
  {
    "id": "abcdef29-fde6-4ebe-8052-3151c1e2dab0",
    "key": "sample-cat-image.jpg",
    "assigneeId": "abcdefdznmNlJ3JA4WqaqzSYjDZ2",
    "assigneeName": "Admin",
    "status": "submitted",
    "reviewAssigneeId": "none",
    "reviewAssigneeName": "Unassigned",
    "reviewStatus": "notReviewed",
    "projectId": "abcdefb0-359f-4552-8d85-54710ea989f3",
    "datasetId": "abcdef52-31b6-428c-91da-f377864966d7",
    "labels": [
      {
        "id": "abcdef1a-8ff2-4d74-9e21-46c69a09a805",
        "shortId": "otc",
        "type": "bbox",
        "code": "cat",
        "name": "Cat",
        "color": "#FF0000",
        "metadata": [],
        "points": [
          {
            "x": 162.85714285714286,
            "y": 36.66666666666667
          },
          {
            "x": 916.1904761904761,
            "y": 608.0952380952381
          }
        ]
      }
    ],
    "duration": 22,
    "createdAt": "2020-08-23T21:57:11.539+09:00[SYSTEM]",
    "updatedAt": "2020-08-23T21:58:12.826+09:00[SYSTEM]"
  }
]
```

> We also support CSV output and others. If you would like to change the output format, please contact us.

### Delete Project

Finally, you can delete the project you just created using the following procedure

1. Click on "Settings" in the side menu
2. Click the "Delete Project" button at the bottom of the screen.
