# Collaboration

FastLabel includes a variety of collaboration features to help creators solve problems and make it easier for managers to manage progress.

## Task Allocation

In some cases, when labeling a large amount of data, you may want to specify the person in charge of the task in advance. If you want to change the person in charge of the task, you can do so in the following way

> In order to change the person in charge of a task, you must have more than reviewer privileges.

1. Open the project
2. Select "Tasks" from the left side menu
3. Select the task for which you want to change the person in charge
4. Click the "Edit" button.
5. Specify the person in charge and click the "Update Task" button.

![task-allocation](images/task-allocation.png ":size=600x350")

## Share Task

FastLabel supports the ability to share other users' tasks. The shared task is read-only and no other user can edit the label. You can share a task by following the steps below

4. Move to the annotation screen.
5. Click on the "Share Task" icon in the upper right corner of the side menu.
6. The URL will be copied, then you can share with other users in your chat tool.

## Add Comment

When you are annotating an image, it is sometimes difficult to tell detailed points to others in writing. By using the comment function, you can freely add text and paint on the annotation screen, enabling you to give detailed feedback to the creator. Follow these steps to add a comment to a task.

1. Move to the annotation screen.
2. Click on the "Add Comment" icon in the upper right corner of the side menu.
3. Select a commenting method and add a comment on the image

Click the "Add Comment" icon again if you want to leave comment mode and label it.

![comment](images/comment.png ":size=600x350")
