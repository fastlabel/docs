# Authority

You can set the privileges for each user. Depending on the authority, you can use different functions.

## Type of Authority

There are three types of authorities

- Creator
- Reviewer
- Admin

## Available Features

| Category  | Feature            |      Creator       |    Reviewer    | Admin |
| --------- | ------------------ | :----------------: | :------------: | :---: |
| Workspace | Project Management |   △（Read Only）   | △（Read Only） |   ◯   |
|           | Member Management  |         ×          |       ×        |   ◯   |
| Project   | Labelling          |         ◯          |       ◯        |   ◯   |
|           | Review             |         ×          |       ◯        |   ◯   |
|           | Task Management    | △（Own Task Only） |       ◯        |   ◯   |
|           | Dataset Management |         ×          |       ×        |   ◯   |
|           | Class Management   |         ×          |       ×        |   ◯   |
