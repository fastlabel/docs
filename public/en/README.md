# FastLabel

FastLabel is an annotation tool that enables seamless collaboration between teams, models and label services. It's web-based and requires no cumbersome setup. The intuitive UI makes it easy for anyone to get started with labeling.

## Outline

- [Quick Start](/quick-start/)
- [Labelling](/labelling/)
- [Review](/review/)
- [Collboration](/collabo/)
- [Authority](/authority/)
