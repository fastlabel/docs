# Labelling

FastLabel supports several annotation types and describes how to label each one.

## Bounding Box (BBox)

If you want to label with a bbox, follow the steps below.

1. Select a class from the right side menu
2. Hold down the start point with the mouse and drag
3. Release the press with the mouse at the end point

![bbox](images/bbox.png ":size=600x350")

## Polygon

If you want to label with a polygon, follow the steps below.

1. Select a class from the right side menu
2. Click on the starting point
3. Click on the next point (Repeat)
4. Click on the start point to finish labelling

> You can easily draw a straight line by moving the mouse while holding down the Shift key.

![polygon](images/polygon.png ":size=600x350")

## Line

If you want to label with a line, follow the steps below.

1. Select a class from the right side menu
2. Click on the starting point
3. Click on the end point

> You can easily draw a straight line by moving the mouse while holding down the Shift key.

## Key Point

If you want to label with a key point, follow the steps below.

1. Select a class from the right side menu
2. Click on the point
