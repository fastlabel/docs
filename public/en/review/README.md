# Review

FastLabel includes a review feature to ensure data quality. Creators can check the label content by reviewers.

## Review Authority

Only users with reviewer or higher privileges can review a task.

## Start Review

To start reviewing tasks, please follow these steps.

1. Open the project
2. Click on "Tasks" from the left side menu
3. Select the task you want to review
4. Click on "Review" in the upper right corner of the screen.

![start-review](images/review.png ":size=600x350")

Check the label content and click "Approve" or "Decline" at the bottom right of the screen.

> Declined tasks will be sent back to their creators and will need to be relabeled.
