# 動画

## 動画分類

FastLabel では、動画分類のアノテーションをサポートしており、ラベルの設定方法を記載します。

### ファイルタイプ

現在サポートしているファイルタイプは以下です。

- MP4

> 他のファイルタイプを利用したい場合はご連絡ください。

### クラスを設定

動画分類では、エディターを直接更新してクラスを定義します。

![bbox](images/video-classification.png ":size=600x350")

### サンプル定義

- セレクトボックス

```json
{
  "classifications": [
    {
      "value": "sample_c",
      "type": "select",
      "name": "Sample",
      "options": [
        {
          "title": "A",
          "value": "sample_a"
        },
        {
          "title": "B",
          "value": "sample_b"
        },
        {
          "title": "C",
          "value": "sample_c"
        }
      ]
    }
  ]
}
```

- ラジオボックス

```json
{
  "classifications": [
    {
      "value": "sample_c",
      "type": "radio",
      "name": "Sample",
      "options": [
        {
          "title": "A",
          "value": "sample_a"
        },
        {
          "title": "B",
          "value": "sample_b"
        },
        {
          "title": "C",
          "value": "sample_c"
        }
      ]
    }
  ]
}
```
